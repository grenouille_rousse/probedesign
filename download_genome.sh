## download the genome from ncbi ftp
wget --recursive --no-host-directories nH --cut-dirs=6 ftp://ftp.ncbi.nih.gov/genomes/genbank/vertebrate_other/Rana_temporaria/representative/GCA_009802015.1_UCB_Rtem_1.0/ -P ./genome/
cd genome
## check if files are not corrupted
md5sum -c md5checksums.txt