## download reference Rana temporaria mitochondrion FASTA file
#esearch -db Nucleotide -query "rana temporaria mitochondrion" | efilter -query "complete genome"
efetch -format fasta -id NC_042226.1 -db Nucleotide > mitogenome/NC_042226.1.fas