# Create a chrom.list comprising the first three linkage groups
gunzip -d genome/GCA_009802015.1_UCB_Rtem_1.0_genomic.fna.gz
samtools faidx genome/GCA_009802015.1_UCB_Rtem_1.0_genomic.fna
# Compress genome
gzip genome/GCA_009802015.1_UCB_Rtem_1.0_genomic.fna
# Generate chrom.list from fai
cat genome/GCA_009802015.1_UCB_Rtem_1.0_genomic.fna.fai | cut -f 1 > genome/chrom.list

# predict number of loci
## ddrad PstI MseI size selection 300+-50
mkdir loci_ddrad_msei_psei
radinitio --tally-rad-loci --genome genome/GCA_009802015.1_UCB_Rtem_1.0_genomic.fna.gz \
--chromosomes genome/chrom.list --out-dir loci_ddrad_msei_psei/ \
--library-type ddRAD --enz MseI --enz2 PstI --insert-mean 300 --insert-stdev 50
## ddrad SbfI MseI size selection 300+-50
mkdir loci_ddrad_msei_sbfi
radinitio --tally-rad-loci --genome genome/GCA_009802015.1_UCB_Rtem_1.0_genomic.fna.gz \
--chromosomes genome/chrom.list --out-dir loci_ddrad_msei_psei/ \
--library-type ddRAD --enz MseI --enz2 SbfI --insert-mean 300 --insert-stdev 50
## ddrad PstI MspI size selection 300+-50
mkdir loci_ddrad_mspi_psei
radinitio --tally-rad-loci --genome genome/GCA_009802015.1_UCB_Rtem_1.0_genomic.fna.gz \
--chromosomes genome/chrom.list --out-dir loci_ddrad_mspi_psei/ \
--library-type ddRAD --enz MspI --enz2 PstI --insert-mean 300 --insert-stdev 50
## ddrad SbfI MspI size selection 300+-50
mkdir loci_ddrad_mspi_sbfi
radinitio --tally-rad-loci --genome genome/GCA_009802015.1_UCB_Rtem_1.0_genomic.fna.gz \
--chromosomes genome/chrom.list --out-dir loci_ddrad_mspi_sbfi/ \
--library-type ddRAD --enz MspI --enz2 SbfI --insert-mean 300 --insert-stdev 50


## count number of loci
### mseI pstI
zgrep -c 'kept' loci_ddrad_msei_sbfi/ref_loci_vars/reference_rad_loci.stats.gz 
### mseI sbfI
zgrep -c 'kept' loci_ddrad_msei_psti/ref_loci_vars/reference_rad_loci.stats.gz 
### mspI pstI
zgrep -c 'kept' loci_ddrad_mspi_psei/ref_loci_vars/reference_rad_loci.stats.gz 
### mspI sbfI
zgrep -c 'kept' loci_ddrad_mspi_sbfi/ref_loci_vars/reference_rad_loci.stats.gz 